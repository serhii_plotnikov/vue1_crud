/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
        el: '#app',
        template: `<div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h3>Add new user: </h3>
                <button class="btn btn-primary" data-toggle="modal" data-target="#add-modal">Add user</button>
                <!-- Modal add user -->
                <div class="modal fade" id="add-modal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add new user</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" v-model="name" placeholder="Enter name">
                          </div>
                          <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" v-model="email" placeholder="Enter email">
                          </div>
                          <input type="file" @change="onImageChange">
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addUser">Create</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-sm-8">
                   <table class="table table-bordered">
                   <caption style="caption-side: top">All users</caption>
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Avatar</th>
                          <th scope="col">Edit</th>
                          <th scope="col">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for='(user,index) in users' :key="user.id">
                          <th scope="row">{{user.id}}</th>
                          <td>{{user.name}}</td>
                          <td>{{user.email}}</td>
                          <td><img :src="user.avatar" alt=""></td>
                          <td class="align-middle">
                            <button class="btn btn-primary" @click="showUpdateModalForm(user.id,index)"
                            data-toggle="modal" data-target="#update-modal">Edit</button>
                          </td>
                          <td class="align-middle">
                            <button class="btn btn-primary" @click="deleteUser(user.id)">Delete</button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
                  <!--Update modal-->
                  <div class="modal fade" id="update-modal" tabindex="-1" role="dialog" 
                        aria-labelledby="update-modal" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Edit user</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form>
                              <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" v-model="name" placeholder="Enter name">
                              </div>
                              <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" v-model="email" placeholder="Enter email">
                              </div>
                              <div class="form-group">
                                <img :src="image" alt="">
                                <input type="hidden">
                              </div>
                              <input type="file"  @change="onImageChange">
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" @click="editUser()">Edit</button>
                          </div>
                        </div>
                      </div>
                   </div>
        <!--Search panel-->
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-3">
                            <h5>Search</h5>
                         </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="search">Name or email</label>
                                <input type="text" class="form-control"  v-model="searchParam" id="search" 
                                    placeholder="Search name or email...">
                             </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="search" id="search_name" 
                              v-model="criterion" value="name" checked>
                              <label class="form-check-label" for="search_name">
                                Search by name
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="search" id="search_email"
                                v-model="criterion" value="email">
                              <label class="form-check-label" for="search_email">
                                Search by email
                              </label>
                            
                            </div>
                        </div>
                        <div style="margin-top: 30px;" class="col-sm-3">
                         <button class="btn btn-primary" @click="searchUser">Search</button>
                        </div>
                    </div>
                    <div v-if="card!==null" class="row">
                        <div class="card" style="width: 18rem;">
                          <img :src="card.avatar" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">{{card.name}}</h5>
                            <p class="card-text">{{card.email}}</p>
                          </div>
                        </div>
                    </div>
                    <div v-else-if="error!==null">
                        <div class="alert alert-danger" role="alert">
                            {{error}}
                        </div>
                    </div>
                </div>
            </div>
    </div>`,
        data: {
            id: null,
            index: null,
            name: '',
            email: '',
            avatar: '',
            users: [],
            image: '',
            criterion: null,
            searchParam: '',
            card: null,
            error: null
        },
        created() {
            this.getUsers();
        },
        methods: {
            getUsers: function () {
                fetch('api/users')
                    .then(response => response.json())
                    .then(response => {
                        this.users = response.data;
                    });
            },
            deleteUser: function (id) {
                fetch(`/api/users/${id}`, {
                    method: 'DELETE'
                }).then(response => response.json())
                    .then(response => {
                        if (response.result === 'success') {
                            this.users = this.users.filter((user) => user.id !== id);
                        }
                    });
            },
            onImageChange: function (e) {
                this.avatar = e.target.files[0];
            },
            addUser: function () {
                let formData = new FormData();
                formData.append('name', this.name);
                formData.append('email', this.email);
                formData.append('avatar', this.avatar);

                fetch('/api/users', {
                    method: 'POST',
                    body: formData
                }).then(response => response.json())
                    .then(response => {
                        let user = response.data;
                        this.$set(this.users, this.users.length, user);
                        this.name = '';
                        this.email = '';
                        this.avatar = '';
                        $('#add-modal').modal('hide');
                    });
            },
            searchUser: function () {
                if (this.searchParam === '' || this.criterion === '') {
                    this.card = null;
                    this.error = "Fill all fields"
                } else {
                    axios.get('/api/users/search', {
                        params: {
                            search: this.searchParam,
                            criterion: this.criterion
                        }
                    }).then((response) => {
                        if (response.data.status === 404) {
                            this.card = null;
                            this.error = response.data.result;
                        } else {
                            let data = response.data.data;
                            this.error = null;
                            this.card = {
                                name: data.name,
                                email: data.email,
                                avatar: data.avatar
                            };
                        }
                    });
                }
            },
            showUpdateModalForm: function (userId, arrayIndex) {
                let currentUser = this.users[arrayIndex];
                this.id = userId;
                this.index = arrayIndex;
                this.name = currentUser.name;
                this.email = currentUser.email;
                this.image = currentUser.avatar;
                this.avatar = currentUser.avatar;
            },
            editUser: function () {
                let formData = new FormData();
                formData.append('id', this.id);
                formData.append('name', this.name);
                formData.append('email', this.email);
                formData.append('avatar', this.avatar);

                axios.post('/api/users/edit', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(response => {
                    let user = response.data.data;
                    this.$set(this.users, this.index, user);
                    this.index = null;
                    this.id = null;
                    this.name = '';
                    this.email = '';
                    this.avatar = '';
                    $('#update-modal').modal('hide');
                });
            }
        }
    })
;

