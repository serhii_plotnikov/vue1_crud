<?php

namespace App\Http\Controllers\API;

use App\Exceptions\UserNotFoundException;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Repositories\Contracts\IUserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function index()
    {
        return UserResource::collection($this->userRepository->findAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return UserResource
     */
    public function store(StoreUserRequest $request)
    {
        $data = $request->validated();
        $file = $data['avatar'];
        $path = Storage::url($file->storeAs('images', $file->getClientOriginalName()));
        $user = $this->userRepository->create($path, $data['name'], $data['email']);
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return UserResource
     */
    public function showByCriterion(Request $request)
    {
        $search = $request->query('search');
        $criterion = $request->query('criterion');
        try {
            $user = $this->userRepository->findByCriterion($search, $criterion);
        } catch (UserNotFoundException $exception) {
            return response()->json([
                'result' => $exception->getMessage(),
                'status' => 404
            ]);
        }
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return UserResource
     */
    public function edit(UpdateUserRequest $request)
    {
        $data = $request->validated();
        try {
            $file = $data['avatar'];
            $path = (strpos($file, 'http') !== false) ? $file :
                Storage::url($file->storeAs('images', $file->getClientOriginalName()));
            $user = $this->userRepository->update($data, $path);
        } catch (UserNotFoundException $exception) {
            return response()->json([
                'result' => $exception->getMessage(),
                'status' => 404
            ]);
        }
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->userRepository->delete($id);
        return response()->json(['result' => 'success']);
    }

    public function show($id)
    {
        return new UserResource($this->userRepository->findById($id));
    }
}
