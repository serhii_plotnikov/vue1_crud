<?php

namespace App\Repositories\Contracts;

use App\User;
use Illuminate\Support\Collection;

interface IUserRepository
{
    public function findAll(): Collection;

    public function delete(int $id): void;

    public function create(string $path, string $name, string $email): User;

    public function findByCriterion(string $searchParam, string $criterion): User;

    public function update(array $params,string $path): User;

    public function findById(int $id): User;
}