<?php


namespace App\Repositories;

use App\Exceptions\UserNotFoundException;
use App\Repositories\Contracts\IUserRepository;
use App\User;
use Illuminate\Support\Collection;

class UserRepository implements IUserRepository
{
    public function findAll(): Collection
    {
        return User::all();
    }

    public function delete(int $id): void
    {
        User::destroy($id);
    }

    public function create(string $path, string $name, string $email): User
    {
        return User::create([
            'name' => $name,
            'email' => $email,
            'avatar' => $path
        ]);
    }

    public function update(array $params, string $path): User
    {
        $user = User::find($params['id']);
        if (!$user) {
            throw new UserNotFoundException("User with id=${params['id']} not found");
        }
        $user->name = $params['name'];
        $user->email = $params['email'];
        $user->avatar = $path;
        $user->save();
        return $user;
    }

    public function findByCriterion(string $searchParam, string $criterion): User
    {
        if ($criterion === 'email') {
            $user = User::where('email', $searchParam)->first();
        } else {
            $user = User::where('name', $searchParam)->first();
        }
        if (!$user) {
            throw new UserNotFoundException('User doesn\'t exist');
        }
        return $user;
    }

    public function findById(int $id): User
    {
        return User::find($id);
    }
}